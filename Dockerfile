# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

FROM debian:11

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -qq && apt-get install -y --no-install-recommends apt-utils locales make
RUN locale-gen

ENV project pim435
COPY . /usr/local/opt/${project}/src/${project}/
WORKDIR /usr/local/opt/${project}/src/${project}/
RUN make setup/debian

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

ENTRYPOINT [ "/usr/bin/make" ]
CMD [ "run" ]
